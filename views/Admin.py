# coding: utf-8
import sys
sys.path.append('../')

import time
from leancloud import LeanCloudError, Query
from flask import Blueprint, flash, render_template, request, redirect
from flask.ext.login import login_user, logout_user, current_user, login_required
from forms.UserForm import SignUpForm, LoginForm, ProfileForm
from forms.CourseForm import NewCourseForm
from models.models import Course, UserProfile

adminView = Blueprint('admin', __name__)

admin_mode = {
    "user": 0,
    "yue": 1,
    "qiu": 2,
    "public": 3,
    "activity": 4
}

@adminView.route('/manage/<mode>', methods=['GET'])
@login_required
def userManage(mode):
    admin_mode = 0
    if mode in admin_mode:
        option_mode = admin_mode[mode]
    user_list = None
    try:
        user_list = Query(UserProfile)
    except LeanCloudError, e:
        pass
    return render_template("admin_user.html", user_list = user_list, option_mode = option_mode)

@adminView.route('/manage/<mode>', methods=['GET'])
@login_required
def
