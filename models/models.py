# coding: utf-8

from leancloud import Object

class UserProfile(Object):
	""" user info """

	@property
	def user(self):
	    return self.get('user')

	@user.setter
	def user(self, user):
		return self.set('user', user)
	
	@property
	def realName(self):
	    return self.get('realName')
	
	@realName.setter
	def realName(self, name):
		return self.set('realName', name)

	@property
	def gender(self):
	    return self.get('gender')
	
	@gender.setter
	def gender(self, gender):
		return self.set('gender', gender)

	@property
	def school(self):
	    return self.get('school')

	@school.setter
	def school(self, school):
		return self.set('school', school)
	
	@property
	def major(self):
	    return self.get('major')

	@major.setter
	def major(self, major):
		return self.set('major', major)

	@property
	def about(self):
	    return self.get('about')

	@about.setter
	def about(self, about):
		return self.set('about', about)

	@property
	def avatar(self):
	    return self.get('avatar')

	@avatar.setter
	def avatar(self, avatar):
		return self.set('avatar', avatar)

class Course(Object):
	""" course info """

	@property
	def title(self):
	    return self.get('title')

	@title.setter
	def title(self, title):
		return self.set('title', title)

	@property
	def info(self):
	    return self.get('info')

	@info.setter
	def info(self, info):
		return self.set('info', info)

	@property
	def time(self):
	    return self.get('time')

	@time.setter
	def time(self, time):
		return self.set('time', time)

	@property
	def location(self):
	    return self.get('location')

	@location.setter
	def location(self, location):
		return self.set('location', location)

	@property
	def mode(self):
		# course mode, YUE=0 / QIU=1
	    return self.get('mode')
	
	@mode.setter
	def mode(self, mode):
		return self.set('mode', mode)

	@property
	def user(self):
	    return self.get('user')

	@user.setter
	def user(self, user):
		return self.set('user', user)

class Tag(Object):
	""" course tag info """
	
	@property
	def name(self):
	    return self.get('name')

	@name.setter
	def name(self, name):
		return self.set('name', name)

class Activity(Object):
	""" Activity info """
	
	@property
	def title(self):
	    return self.get('title')

	@title.setter
	def title(self, title):
		return self.set('title', title)

	@property
	def link(self):
	    return self.get('link')

	@link.setter
	def link(self, link):
		return self.set('link', link)

	@property
	def image(self):
	    return self.get('image')

	@image.setter
	def image(self, image):
		return self.set('image', image)

class CourseComment(Object):
	""" course comment """

	@property
	def content(self):
	    return self.get('content')

	@content.setter
	def content(self, content):
		return self.set('content', content)

	@property
	def fromUid(self):
	    return self.get('fromUid')

	@fromUid.setter
	def fromUid(self, fromUid):
		return self.set('fromUid', fromUid)

	@property
	def toCid(self):
	    return self.get('toCid')

	@toCid.setter
	def toCid(self, toCid):
		return self.set('toCid', toCid)