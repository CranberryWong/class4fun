# coding: utf-8

from flask_wtf import Form
from wtforms import StringField, PasswordField, BooleanField
from wtforms.validators import DataRequired, Email, Length, Regexp

class SignUpForm(Form):
	""" validate sign up:
	username: 6 < size < 20
	password: 6 < size < 20
	email: email regexp
	phonenumber: phone regexp
	"""
	username = StringField('usename', validators=[DataRequired(), Length(min=6, max=20)])
	password = PasswordField('password', validators=[DataRequired(), Length(min=6, max=20)])
	email = StringField('email', validators=[Email()])
	phone = StringField('phone', validators=[Regexp(regex=r'(1)(3\d|4[5,7]|5[0-3,5-9]|8[0,2,3,6-9])\D*(\d{4})\D*(\d{4})$')])

class LoginForm(Form):
	""" validate login:
	username: DataRequired
	password: DataRequired
	remember: optional
	"""
	username = StringField('usename', validators=[DataRequired()])
	password = PasswordField('password', validators=[DataRequired()])
	remember = BooleanField('remember')

class ProfileForm(Form):
	""" validate profile """
	realname = StringField('realname', validators=[DataRequired(),Length(min=2, max=10)])
	gender = BooleanField('gender')
	school = StringField('school', validators=[DataRequired(),Length(min=2, max=50)])
	major = StringField('major', validators=[DataRequired(),Length(min=2, max=50)])
	about = StringField('about', validators=[Length(min=0, max=200)])
		